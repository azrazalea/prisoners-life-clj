(ns prisoners-life.core
  (gen-class)
  (:require crypto.random
            [lanterna.screen :as s]
            [clojure.string :as str]
            clojure.pprint
            [clojure.tools.cli :refer [parse-opts]]))

(defrecord Cell [coords
                 name
                 display
                 strategy
                 score
                 state
                 helped])

(defn generate-name []
  (crypto.random/base64 64))

(defn generate-cell
  "Generates a new cell using the given row, column, and strategy.
  Name is randomized"
  [row column strategy]
  (Cell. [row column] (generate-name) (:display (meta strategy)) strategy 0 {} 0))

(defn create-random-row
  "Creates a random row of width elements using given strategies and row number
  Randomly picks strategy per a cell"
  [width strategies row]
  (vec (map-indexed
        (fn [column _]
          (rand-nth [(generate-cell row column (rand-nth strategies)) nil]))
        (repeat width nil))))

(defn create-random-grid
  "Creates a random grid with width rows and columns"
  [width strategies]
  (vec (map-indexed
        (fn [row _]
          (create-random-row width strategies row))
        (repeat width nil))))

(defmacro add-coords
  "Little shortcut to add two coordinates"
  [a b]
  `(map + ~a ~b))

(defn wrap-coords
  "Calculate coords after wrapping on grid edges"
  [grid [x y]]
  (let [x-size (count (first grid))
        y-size (count grid)]
    [(mod (+ x x-size) x-size) (mod (+ y y-size) y-size)]))

(defn get-in-wrap
  "A get-in function that wraps on grid edges."
  [grid coords]
  (get-in grid (wrap-coords grid coords)))

;;; These functions both need to use neighbors
;;; Seemed like a good way to do this
(let [neighbors [[-1 1]
                 [0 1]
                 [1 1]
                 [1 0]
                 [1 1]
                 [0 -1]
                 [-1 -1]
                 [-1 0]]]

  (defn get-neighbors
    "Grabs all neighbors, returns a vector of them"
    [grid coord]
    (map #(get-in-wrap grid (add-coords %1 coord)) neighbors))


  (defn get-neighbors-with-coords
    "Grabs all neighbors with a vector of [neighbor-coords neighbors]"
    [grid coord]
    (let [neighbor-coords (map #(add-coords coord %1) neighbors)]
      (map vector neighbor-coords (map (partial get-in-wrap grid) neighbor-coords)))))

(defn get-relationships
  "Return a sequence containing vectors of all relationships a cell has"
  [grid coord]
  (let [neighbors (get-neighbors grid coord)
        element (get-in-wrap grid coord)]
    (map #(hash-set element %1) neighbors)))

(defn get-all-relationships
  "Create a set of all relationships between all cells of grid"
  [grid]
  (filter #(not (contains? %1 nil))
          (set (flatten (map-indexed (fn [index row]
                                       (map-indexed
                                        (fn [column element]
                                          (get-relationships grid [index column])) row))
                                     grid)))))

(defn inc-cell-score
  "Increment the score of the cell represented by the given coords by value"
  [grid [x y] value]
  (update-in grid [x y :score] + value))

(defn update-helped
  "Increment helped for every helper, don't do anything for defectors"
  [current update]
  (if (= update :coop)
    (inc current)
    current))

(defn cell-scores
  "Calculate the scores of the cells, return as vector"
  [results]
  (case results
    [:coop :defect] [0 5]
    [:coop :coop]   [3 3]
    [:defect :coop] [5 0]
    [1 1]))

(defn update-state
  "Updates the state of the grid to include the most recent results from the cell relationship"
  [grid cell-one cell-two result-one result-two]
  (let [[x1 y1] (:coords cell-one)
        [x2 y2] (:coords cell-two)
        name1 (:name cell-one)
        name2 (:name cell-two)
        [score-one score-two] (cell-scores [result-one result-two])]
    (->
     grid
     (update-in [x1 y1 :state name2] conj result-two)
     (update-in [x2 y2 :state name1] conj result-one)
     (update-in [x1 y1 :helped] update-helped result-two)
     (update-in [x2 y2 :helped] update-helped result-one)
     (inc-cell-score (:coords cell-one) score-one)
     (inc-cell-score (:coords cell-two) score-two))))

(defn run-transformation
  "Transforms grid based on a single relationship"
  [grid relationship]
  (let [cell-one (first relationship)
        cell-two (second relationship)
        result-one ((:strategy cell-one) cell-one cell-two)
        result-two ((:strategy cell-two) cell-two cell-one)]
    (update-state grid cell-one cell-two result-one result-two)))

(defn run-transformations
  "Transforms grid based on all relationships"
  [grid]
  (let [relationships (get-all-relationships grid)]
    (reduce run-transformation grid relationships)))

(defn kill-cell
  "Kills the cell if it's helped value is lower than min-helped"
  [min-helped cell]
  (when (and cell (>= (:helped cell) min-helped))
    cell))

(defn clear-helped
  "Clears the helped value of all cells in grid"
  [grid]
  (vec (map #(vec (map (fn [cell] (when cell (assoc cell :helped 0))) %1)) grid)))

(defn kill-cells
  "Runs kill-cell on all cells in grid"
  [min-helped grid]
  (vec (map #(vec (map (partial kill-cell min-helped) %1)) grid)))

(defn run-reproduction
  "Runs reproduction algorithm on cell"
  [reproduce-score grid cell]
  (let [neighbors (get-neighbors-with-coords grid (:coords cell))
        possible  (filter  #(nil? (second %1)) neighbors)]
    (if  (>= reproduce-score (:score cell)) ; Ready to reproduce?
      (if-not (empty? possible) ; Overcrowded?
        (let [[x y] (wrap-coords grid (ffirst possible))] ; Baby making
          (->
           grid
           (assoc-in [x y] (generate-cell x y (:strategy cell)))
           (assoc-in (flatten [(:coords cell) :score]) 0)))
        (assoc-in grid (:coords cell) nil))  ; If you cannot reproduce due to crowding, die
      grid)))

(defn run-reproductions
  "Run reproduction algorithm on all cells in grid"
  [reproduce-score grid]
  (reduce (partial run-reproduction reproduce-score) grid (remove nil? (flatten grid))))

(defn transform-grid
  "Runs all recommendations needed to get the next frame of a grid and returns result"
  [grid reproduce-score min-helped]
  (->>
   grid
   (run-transformations)
   (kill-cells min-helped)
   (run-reproductions reproduce-score)
   (clear-helped)))

(defmacro def-strat
  "Creates a function with associated metadata, used for strategies"
  [name args display-char form]
  `(def ~name (with-meta (fn ~name ~args ~form) {:display ~display-char})))

;; Always cooperates
(def-strat chump-strategy [me them]
  "*"
  :coop)

;; Always defects
(def-strat mean-strategy [me them]
  "!"
  :defect)

;; Does whatever the other guy did last, defect default
(def-strat tit-for-tat-strategy-defect-first [me them]
  "&"
  (let [last-move (last ((symbol (:name them)) (:state me)))]
    (if last-move
        last-move
        :defect)))

;; Does whatever the other guy did last, coop default
(def-strat tit-for-tat-strategy-coop-first [me them]
  "#"
  (let [last-move (last ((symbol (:name them)) (:state me)))]
    (if last-move
      last-move
      :coop)))

;; Always defects if the other guy ever defected
(def-strat grudge-strategy [me them]
  "^"
  (if (contains? ((symbol (:name them)) (:state me)) 1)
    :coop
    :defect))

;; Randomly either defects or coops
(def-strat random-strategy [me them]
  "@"
  (rand-nth [0 1]))

(defn grid-row-to-string
  "Formats a grid row as a string"
  [row]
  (str/join (map (fn [cell]
                   (if (nil? cell)
                     " "
                     (:display cell))) row)))

(defn grid-to-strings
  "Formats a grid as a sequence of strings"
  [grid]
  (map grid-row-to-string grid))

(defn draw-grid
  "Draws a grid to the screen"
  [scr grid]
  (s/put-sheet scr 0 0 (grid-to-strings grid))
  (s/redraw scr))

(def cli-options
  [["-h" "--help"]
   ["-s" "--size SIZE" "Grid size"
    :default 50
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 256) "Must be a number between 0 and 256 exclusive"]]
   ["-r" "--reproduce-score REPRODUCE-SCORE" "Score required to reproduce"
    :default 40
    :parse-fn #(Integer/parseInt %)
    :validate [#(> 0 %) "Must be a number greater than 0"]]
   ["-c" "--coop-value COOP-VALUE" "Minimum number of cooperations per a frame to survive"
    :default 3
    :parse-fn #(Integer/parseInt %)
    :validate [#(<= 0 % 8) "Must be a number between 0 and 8 inclusive"]]])

(defn usage
  "Returns the usage string"
  [options-summary]
  (str/join \newline ["Simulates the Prisoner's Life (see README and LICENSE)"
                      ""
                      "Usage: prisoners-life [options]"
                      ""
                      "Options:"
                      options-summary]))

(defn error-msg
  "Returns a string containing all errors"
  [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (str/join \newline errors)))

(defn exit
  "Exit the program"
  [status msg]
  (println msg)
  (System/exit status))

(defn -main
  "It is main function, mmmkay?"
  [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)
        {:keys [size reproduce-score coop-value]} options
        strategies [chump-strategy
                    mean-strategy
                    tit-for-tat-strategy-defect-first
                    tit-for-tat-strategy-coop-first
                    grudge-strategy
                    random-strategy]]

    (cond (:help options) (exit 0 (usage summary))
          errors (exit 1 (error-msg errors)))

    (let [scr (s/get-screen :swing {:cols size :rows size})]
      (s/start scr)

      (loop [grid (create-random-grid size strategies)]
        (s/clear scr)
        (draw-grid scr grid)

        (if (case (s/get-key-blocking scr {:interval 100 :timeout 1000})
              (\q \Q) false
              (\p \P) (do
                        (s/get-key-blocking scr)
                        true)
              (\d \D) (do
                        (spit "/tmp/grid.txt" (with-out-str (clojure.pprint/pprint grid)) :append true)
                        true)
              true)

          (recur (transform-grid grid reproduce-score coop-value))
          (s/stop scr))))))
