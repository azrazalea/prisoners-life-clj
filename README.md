prisoners-life-clj
==================

Clojure implementation of a prisoners dilemma/game of life mashup.

Rules
=====
1. Someone must cooperate with you (keep silent in classic Prisoner's Dilemma) each round or you die.
2. Classic prisoner's dilemma scoring, You get 1 point if both you and your neighbor refuse to cooperate, 3 if you both cooperate, and 5 if you refuse to cooperate but your neighbor cooperates.
3. When you hit X score(configurable) you get to reproduce, creating a new cell with your strategy.
