(defproject prisoners-life "0.1.0-SNAPSHOT"
  :description "Prisoners dilemma life game"
  :url "http://gitlab.com/azrazalea/prisoners-life-clj/"
  :license {:name "BSD 3-Clause License"
            :url "http://opensource.org/licenses/BSD-3-Clause"}
  :dependencies [[org.clojure/clojure "1.7.0-alpha3"]
                 [crypto-random "1.2.0"]
                 [clojure-lanterna "0.9.4"]
                 [org.clojure/tools.cli "0.3.1"]]
  :main ^:skip-aot prisoners-life.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
